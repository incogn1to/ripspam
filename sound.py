#!/usr/bin/env python

import pygame
import time

aprove_file = 'sounds/aproved.wav'
reject_file = 'sounds/bear.wav'
destroy_file = 'sounds/warning.wav'
process_file = 'sounds/computer.wav'

pygame.mixer.init()

def play_sound(file):
    pygame.mixer.music.load(file)
    pygame.mixer.music.set_volume(1.0)
    pygame.mixer.music.play()
 
def aproved():
    play_sound(aprove_file)

def rejected():
    play_sound(reject_file)
 
def destroyed():
    play_sound(destroy_file)

def processing():
    play_sound(process_file)
 
