#!/usr/bin/env python

import RPi.GPIO as GPIO
import time
import img2text
import datetime
import picamera
import sys
import cmp

GPIO.setwarnings(False)

step_pin = 24
dir_pin = 22
opto_pin = 17
servo_pin = 26

whiteListPath = "lists/whitelist.txt"
blackListPath = "lists/blacklist.txt"
destroyListPath = "lists/destructionlist.txt"

GPIO.setmode(GPIO.BCM)

GPIO.setup(step_pin, GPIO.OUT)
GPIO.setup(dir_pin, GPIO.OUT)
GPIO.setup(opto_pin, GPIO.IN)
GPIO.setup(servo_pin, GPIO.OUT)
GPIO.output(dir_pin, GPIO.LOW)

def angle_duty(angle):
    return angle/18+2

servo = GPIO.PWM(servo_pin, 50)
servo.start(angle_duty(0))

step = GPIO.PWM(step_pin, 50)
step.ChangeFrequency(10e3)

def isMailInserted():
    if GPIO.input(opto_pin) == GPIO.LOW:
        return True
    else:
        return False

def rollMailFullyUp():
    GPIO.output(dir_pin, GPIO.HIGH)
    step.start(50)
    step.ChangeFrequency(10e3)
    time.sleep(1)
    while isMailInserted() == True :
        time.sleep(0.1)
    step.stop()

def rollMailDown():
    GPIO.output(dir_pin, GPIO.LOW)
    step.start(50)
    step.ChangeFrequency(10e3)

def rollMailFullyDown():
    GPIO.output(dir_pin, GPIO.LOW)
    step.start(50)
    step.ChangeFrequency(10e3)
    time.sleep(2)
    step.stop()

def rollStop():
    step.stop()

def takephoto():
    fileName = 'camera/' + str(datetime.datetime.now()) + '.jpg'
    camera.capture(fileName)  
    return fileName

def isInList(words,samplelist):
    return (set(samplelist) & set(words))

def procImg(file):
    wordList = img2text.get_text(file)
    if wordList:
        wordList = list(map(lambda x:x.upper(),wordList))
    return wordList

def allowMail():
    servo.ChangeDutyCycle(angle_duty(180))

def denyMail():
    servo.ChangeDutyCycle(angle_duty(0))
    
def readList(path):
    word_list = []
    file = open(path,'r') 
    for line in file: 
        word_list.append(line)
    word_list = list(map(lambda x:x.upper(),word_list))
    word_list = [unicode(x) for x in word_list]
    return word_list

whiteList = ['mama','grandma']
blackList = ['maksuamet']
destructionList = ['spam','motherinlaw']

try:
    camera = picamera.PiCamera()

    whiteList = readList(whiteListPath)
    blackList = readList(blackListPath)
    destroyList = readList(destroyListPath)

    allowMail()

    while True:
        time.sleep(0.1)
        if isMailInserted(): 
            rollMailDown()
            while isMailInserted() == True :
                time.sleep(0.1)
            rollStop()

            image = takephoto();
            wordList = procImg(image)
            print wordList

            if isInList(wordList,whiteList):
                print "In white list"
                rollMailFullyDown()
            elif isInList(wordList,destructionList):
                print "In Desctruction list"
                denyMail()
                time.sleep(0.5)
                rollMailFullyDown()
                allowMail()
            elif isInList(wordList,blackList):
                print "In black list"
                rollMailFullyUp()
            else:
                print "Not in list. Ignoring"
                rollMailFullyDown()

except KeyboardInterrupt:
    GPIO.cleanup()
except:
    print("Unexpected error:", sys.exc_info()[0])
    raise

