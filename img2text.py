#!/usr/bin/env python
# Copyright 2016 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
This example uses the Google Cloud Vision API to detect text in images, then
analyzes that text using the Google Cloud Natural Language API to detect
entities in the text. It stores the detected entity information in an sqlite3
database, which may then be queried.

After this script has analyzed a directory of images, it outputs some
information on the images' entities to STDOUT. You can also further query
the generated sqlite3 database; see the README for more information.

Run the script on a directory of images to do the analysis, E.g.:
    $ python main.py --input_directory=<path-to-image-directory>

You can try this on a sample directory of images:
    $ curl -O http://storage.googleapis.com/python-docs-samples-tests/language/ocr_nl-images.zip
    $ unzip ocr_nl-images.zip
    $ python main.py --input_directory=images/

"""  # noqa

from curses.ascii import isprint
import argparse
import base64
import contextlib
import logging
import os
import sqlite3
import sys
import time
import picamera
import string

import googleapiclient.discovery
import googleapiclient.errors

#os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'credential json'

BATCH_SIZE = 10

def takephoto():
    camera = picamera.PiCamera()
    camera.capture('camera.jpg')  

class VisionApi(object):
    """Construct and use the Cloud Vision API service."""

    def __init__(self):
        self.service = googleapiclient.discovery.build('vision', 'v1')

    def detect_text(self, filename, num_retries=3, max_results=1):
        """Uses the Vision API to detect text in the given file."""
        batch_request = []
        request = {
            'image': {},
            'features': [{
                'type': 'TEXT_DETECTION',
                'maxResults': max_results,
            }]
        }

        with open(filename, 'rb') as image_file:
            request['image']['content'] = base64.b64encode(
                    image_file.read()).decode('UTF-8')

        batch_request.append(request)

        request = self.service.images().annotate(
            body={'requests': batch_request})

        try:
            text_response = {}
            responses = request.execute(num_retries=num_retries)
            if 'responses' not in responses:
                return {}

            resp = responses.get('responses', [])

            text_anotation = resp[0].get('textAnnotations', [])
            full_text = text_anotation[0]
            text_response = full_text.get('description', [])
            return text_response

        except googleapiclient.errors.HttpError as e:
            logging.error('Http Error for {}: {}'.format(filename, e))
        except KeyError as e2:
            logging.error('Key error: {}'.format(e2))
        except:
            return text_response


def extract_description(texts):
    """Returns text annotations as a single string"""
    document = []

    for text in texts:
        try:
            document.append(text['description'])
            locale = text['locale']
            # Process only the first entry, which contains all
            # text detected.
            break
        except KeyError as e:
            logging.error('KeyError: %s\n%s' % (e, text))
    return (locale, ' '.join(document))


def extract_descriptions(input_filename, texts, text_analyzer):
    """Gets the text that was detected in the image."""
    if texts:
        locale, document = extract_description(texts)
        text_analyzer.add_entities(input_filename, locale, document)
        sys.stdout.write('.')  # Output a progress indicator.
        sys.stdout.flush()
    elif texts == []:
        print('%s had no discernible text.' % input_filename)


def get_text_from_files(vision, input_filenames, text_analyzer):
    """Call the Vision API on a file and index the results."""
    texts = vision.detect_text(input_filenames)
    if texts:
        for filename, text in texts.items():
            extract_descriptions(filename, text, text_analyzer)


def batch(list_to_batch, batch_size=BATCH_SIZE):
    """Group a list into batches of size batch_size.

    >>> tuple(batch([1, 2, 3, 4, 5], batch_size=2))
    ((1, 2), (3, 4), (5))
    """
    for i in range(0, len(list_to_batch), batch_size):
        yield tuple(list_to_batch[i:i + batch_size])


def printable(input):
    return ''.join(char for char in input if isprint(char))

def get_text(input_file):
    # Create a client object for the Vision API
    vision_api_client = VisionApi()
    # Create an object to analyze our text using the Natural Language API
    output = [] 
    if input_file:
        text = vision_api_client.detect_text(input_file)
        if text:
            output = text.split(' ')
            output = [x.strip() for x in output]
    return output

